/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  TouchableOpacity,
} from 'react-native';
import Home from './src/components/Home';
import Trips from './src/components/Trips';
import Favourite from './src/components/Favourite';
import Profile from './src/components/Profile';
import Place from './src/components/Place';
import OnBoarding from './src/components/OnBoarding';
import Register from './src/components/Register';
import PreLoading from "./src/components/PreLoading";
import Login from './src/components/Login';
import Logout from './src/components/Logout';
import Icon from 'react-native-vector-icons/FontAwesome';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createDrawerNavigator, DrawerItems } from 'react-navigation-drawer';
import AsyncStorage from '@react-native-community/async-storage';


const CustomDrawerComponent = props => (
  <SafeAreaView>
    <View style={{
      height: 132,
      backgroundColor:"#60c2a2",
      paddingLeft: 10,
      paddingBottom: 15,
    }}>
      <View style={{
        flex: 1,
        flexDirection: "row",
        alignItems: "flex-end"
      }}>
        <Image
          source={require("./src/assets/images/naseef.jpg")}
          style={{
            height: 52,
            width: 52,
            borderRadius: 25,
            marginRight: 10
          }} />
          <Text style={{
            color: "#fff",
            fontSize: 14
          }}>
            Naseef m
          </Text>
      </View>
    </View>

    <ScrollView>
      <DrawerItems {...props} />
      {/* <TouchableOpacity
        style={styles.menuItem}
        onPress={() => (this.props.navigation.navigate("Profile"))}>
          <Icon name= "power-off" style={styles.menuIcon} />
          <Text style={styles.menuText}>Sign out</Text>
      </TouchableOpacity> */}
    </ScrollView>
  </SafeAreaView>
);

const DrawerNavigation = createDrawerNavigator({
  Home: {
    screen: Home,
    navigationOptions : {
      drawerIcon : ({tintColor}) => (
        <Icon name= "home" size={22} color={tintColor} />
      )
    }
  },
  Trips: {
    screen: Trips,
    navigationOptions : {
      drawerIcon : ({tintColor}) => (
        <Icon name= "car" size={22} color={tintColor} />
      )
    }
  },
  Favourite: {
    screen: Favourite,
    navigationOptions : {
      drawerIcon : ({tintColor}) => (
        <Icon name= "heart" size={22} color={tintColor} />
      )
    }
  },
  Profile: {
    screen: Profile,
    navigationOptions : {
      drawerIcon : ({tintColor}) => (
        <Icon name= "user" size={24} color={tintColor} />
      )
    }
  },
  Logout: {
    screen: Logout,
    navigationOptions : {
      drawerIcon : ({tintColor}) => (
        <Icon name= "power-off" size={24} color={tintColor} />
      )
    }
  },
},
{
  contentComponent: CustomDrawerComponent,
  // initialRouteName: "Home",
  contentOptions: { activeTintColor : "#60c2a2"}
}
);


const TabNavigator = createBottomTabNavigator({
  Home: {
    screen: DrawerNavigation
  },
  Trips: {
    screen: Trips
  },
  Favourite: {
    screen: Favourite
  },
  Profile: {
    screen: Profile
  },
},
{
  defaultNavigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ focused, horizontal, tintColor }) => {
      const { routeName } = navigation.state;
      let iconName;
      if (routeName === 'Home') {
        iconName = `home`;
      } else if (routeName === 'Trips') {
        iconName = `car`;
      }else if (routeName === 'Favourite') {
        iconName = `heart`;
      }else if (routeName === 'Profile') {
        iconName = `user`;
      }
      return <Icon name={iconName} size={22} color={tintColor} />;
    },
  }),
  tabBarOptions: {
    activeTintColor: '#ff2d55',
    inactiveTintColor: 'gray',
    // showLabel: false,
    style: {
      backgroundColor: '#fff',
    },
  },
}
);

const TabContainer = createAppContainer(TabNavigator);

const StackaNvigator = createStackNavigator({
  Home: {
    screen: TabContainer
  },
  // OnBoarding: {
  //   screen: OnBoarding
  // },
  // Register: {
  //   screen: Register
  // },
  // Login: {
  //   screen: Login
  // },
  Place: {
    screen: Place
  },
},
{
  headerMode: "none",
  initialRouteName: "Home",
  // initialRouteName: "OnBoarding",
  navigationOptions: { headerVisible: false }
}
);

// const StackContainer = createAppContainer(StackaNvigator);

// export default class App extends Component {
//   render(){
//     return (
//       <View style={styles.Container}>
//         <StackContainer />
//       </View>
//     );
//   }
// }

const AuthStackNavigator = createStackNavigator(
  {
    OnBoarding: {
      screen: OnBoarding
    },
    Login: {
      screen: Login
    },
    Register: {
      screen: Register
    }
  },
  {
    headerMode: "none",
    initialRouteName: "OnBoarding",
    navigationOptions: { headerVisible: false }
  }
);

export default createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: PreLoading,
      App: StackaNvigator,
      Auth: AuthStackNavigator
    },
    {
      initialRouteName: "AuthLoading"
    }
  )
);

// const MainAppcontainer = createAppContainer(
//   createSwitchNavigator(
//     {
//       AuthLoading: PreLoading,
//       App: StackaNvigator,
//       Auth: AuthStackNavigator
//     },
//     {
//       initialRouteName: "AuthLoading"
//     }
//   )
// );

// export default class App extends Component {
//   render() {
//     return (
//       <MainAppcontainer />
//     ); 
//   }
// }



const styles = StyleSheet.create({
  Container: {
    height: "100%",
    backgroundColor: "#f5f5f5",
  },
  menuItem: {
    flex: 1,
    flexDirection: "row",
    paddingLeft: 20,
    marginTop: 5,
    alignItems: "center",
    marginBottom: 15
  },
  menuIcon: {
    marginRight: 15,
    fontSize: 24,
    color: "#666"
  },
  menuText: {
    paddingLeft: 20,
    color: "#222",
    fontSize: 14,
    fontWeight: "bold"
  }
});
