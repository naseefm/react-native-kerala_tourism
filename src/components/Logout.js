import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

export default class Log extends Component {
  _signOutAsync = async () => {
    await AsyncStorage.clear();
    this.props.navigation.navigate("Auth");
  };
  render(){
    return (
      <View style={styles.Container}>
        <Text style={styles.text}>Are you sure you want to logout this application ?</Text>
        <View style={styles.Buttons}>
          <TouchableOpacity style={styles.buttonYes} onPress={() => this._signOutAsync()}><Text style={styles.buttonText}>Yes</Text></TouchableOpacity>
          <TouchableOpacity style={styles.buttonNo} onPress={() => this.props.navigation.goBack()}><Text style={styles.buttonText}>No</Text></TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  Container: {
    height: "100%",
    backgroundColor: "#f5f5f5",
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    marginVertical: 80,
    textAlign: "center",
    fontSize: 20,
    fontFamily: "WorkSans-Bold",
  },
  Buttons: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    width: "60%",
  },
  buttonYes: {
    borderColor: "#000",
    borderWidth: 1,
    height: 30,
    paddingHorizontal: 30,
    paddingVertical: 5,
    backgroundColor: "#ff2d55",
  },
  buttonNo: {
    borderColor: "#000",
    borderWidth: 1,
    height: 30,
    paddingHorizontal: 30,
    paddingVertical: 5,
    backgroundColor: "#ff2d55",
  },
  buttonText: {
    fontFamily: "WorkSans-Bold",
    color: "#fff",
  }

});