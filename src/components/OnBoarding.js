import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView
} from "react-native";
import { SafeAreaView } from "react-navigation";

var { height, width } = Dimensions.get("window");
var container_width = width - 40;

export default class OnBoarding extends Component {
  render() {
    return (
      <SafeAreaView
        style={{
          backgroundColor: "#F5FCFF",
          flex: 1
        }}
      >
        <ScrollView contentContainerStyle={styles.container}>
          <Text style={styles.title}>Get Started</Text>
          <Image
            style={{
              width: container_width,
              marginVertical: 80
            }}
            source={require("../assets/images/onboaring.png")}
          />
          <View style={styles.buttons}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("Login")}
              style={styles.login}
            >
              <Text style={styles.login_text}>Login now</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("Register")}
            >
              <Text style={styles.signup_text}>Signup with email</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "space-around",
    alignItems: "center",
    paddingVertical: 50
  },
  title: {
    fontWeight: "bold",
    fontSize: 24,
    color: "#3D4A59",
    marginTop: 20
  },
  login: {
    width: container_width,
    justifyContent: "center",
    alignItems: "center",
    height: 50,
    backgroundColor: "#F24A55",
    borderRadius: 8,
    marginBottom: 20
  },
  login_text: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 16
  },
  signup_text: {
    fontSize: 16,
    color: "#8B959A"
  },
  buttons: {
    justifyContent: "center",
    alignItems: "center"
  }
});
