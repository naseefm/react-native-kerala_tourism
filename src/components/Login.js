import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  ScrollView,
  KeyboardAvoidingView
} from "react-native";
import { SafeAreaView } from "react-navigation";
import AsyncStorage from "@react-native-community/async-storage";
import axios from "axios";

var { height, width } = Dimensions.get("window");
var container_width = width - 40;

export default class Login extends Component {
  state = {
    email: "",
    password: "",

    successful_join: false,
    message: "",

    refresh_token: "",
    access_token: "",
    role: ""
  };

  onJoin() {
    var email = this.state.email;
    var password = this.state.password;

    var join_url =
      "http://traveller.demotechpe.talrop.com/api/v1/auth/token/";
    axios
      .post(join_url, {
        username: email,
        password: password
      })
      .then(response => {
        var data = response.data;
        var access_token = data.access;
        var refresh_token = data.refresh;
        var role = data.role;
        this.setState({
          successful_join: true,
          access_token: access_token,
          refresh_token: refresh_token,
          role: role
        });
        this.addAccount();
      })
      .catch(e => {
        console.error(e);
        this.setState({
          message: "An Error Occured, Please try again later."
        });
      });
  }

  async addAccount() {
    try {
      var user = {
        is_verified: "true",
        role: "app_user",
        email: this.state.email,
        access_token: this.state.access_token,
        refresh_token: this.state.refresh_token
      };
      await AsyncStorage.setItem("user", JSON.stringify(user)).then(result => {
        this.props.navigation.navigate("App")
      });
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    return (
      <SafeAreaView
        style={{
          backgroundColor: "#F5FCFF",
          flex: 1
        }}
      >
        <ScrollView contentContainerStyle={styles.container}>
          <KeyboardAvoidingView style={styles.containerStyle} behavior="padding" >
            <Text style={styles.title}>Login Now</Text>
            <Image
              style={{
                width: container_width,
                resizeMode: "contain"
              }}
              source={require("../assets/images/login.png")}
            />

            <TouchableOpacity style={{ marginVertical: 20 }}>
              <Text style={styles.signup_text}>Login with email</Text>
            </TouchableOpacity>

            <View>
              <View style={{ marginBottom: 18 }}>
                <Text
                  style={{
                    color: "#8B959A",
                    marginBottom: 5,
                    marginLeft: 5
                  }}
                >
                  E-mail ID
                </Text>
                <TextInput
                  onChangeText={text =>
                    this.setState({
                      email: text
                    })
                  }
                  returnKeyType="done"
                  style={styles.input}
                  placeholder="E-mail"
                />
              </View>
              <View style={{ marginBottom: 18 }}>
                <Text
                  style={{
                    color: "#8B959A",
                    marginBottom: 5,
                    marginLeft: 5
                  }}
                >
                  Password
                </Text>
                <TextInput
                  secureTextEntry={true}
                  returnKeyType="done"
                  onChangeText={text =>
                    this.setState({
                      password: text
                    })
                  }
                  style={styles.input}
                  placeholder="Password"
                />
              </View>
            </View>

            <TouchableOpacity
              onPress={this.onJoin.bind(this)}
              style={styles.login}
            >
              <Text style={styles.login_text}>Login to my account</Text>
            </TouchableOpacity>
          </KeyboardAvoidingView>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    flexGrow: 1
  },
  container: {
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 50
  },
  title: {
    fontWeight: "bold",
    fontSize: 24,
    color: "#3D4A59",
    marginBottom: 40,
    textAlign: "center"
  },
  login: {
    width: container_width,
    justifyContent: "center",
    alignItems: "center",
    height: 50,
    backgroundColor: "#F24A55",
    borderRadius: 8,
    marginTop: 20
  },
  signup_text: {
    fontSize: 16,
    color: "#8B959A"
  },
  login_text: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 16
  },
  input: {
    height: 40,
    borderColor: "#8B959A",
    borderWidth: 0.5,
    backgroundColor: "#fff",
    width: container_width,
    borderRadius: 5,
    paddingLeft: 10
  }
});