import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  ScrollView,
  KeyboardAvoidingView
} from "react-native";
import { SafeAreaView } from "react-navigation";
import AsyncStorage from '@react-native-community/async-storage';
import axios from "axios";

var { height, width } = Dimensions.get("window");
var container_width = width - 40;

export default class Register extends Component {
  state = {
    first_name: "",
    last_name: "",
    email: "",
    password: "",

    successful_join: false,
    message: "",

    refresh_token: "",
    access_token: "",
    role: ""
  };

  onRegister() {
    var first_name = this.state.first_name;
    var last_name = this.state.last_name;
    var email = this.state.email;
    var password = this.state.password;

    var join_url =
      "http://traveller.demotechpe.talrop.com/api/v1/auth/register/";
    axios
      .post(join_url, {
        first_name: first_name,
        last_name: last_name,
        email: email,
        password: password
      })
      .then(response => {
        var status_code = response.data.StatusCode;

        if (status_code === 6000) {
          var data = response.data.data;
          var access_token = data.access;
          var refresh_token = data.refresh;
          var role = data.role;
          this.setState({
            successful_join: true,
            access_token: access_token,
            refresh_token: refresh_token,
            role: role
          });
          this.addAccount();
        } else if (status_code === 6001) {
          var message = response.data.message;

          this.setState({
            message: message
          });
        }
      })
      .catch(e => {
        console.error(e);
        this.setState({
          message: "An Error Occured, Please try again later."
        });
      });
  }

  async addAccount() {
    console.warn("Hello");
    try {
      var user = {
        first_name: this.state.first_name,
        last_name: this.state.last_name,
        is_verified: "true",
        role: "app_user",
        email: this.state.email,
        access_token: this.state.access_token,
        refresh_token: this.state.refresh_token
      };
      console.warn(user);
      await AsyncStorage.setItem("user", JSON.stringify(user)).then(result => {
        console.warn("Inside async");
        this.props.navigation.navigate("App")
      });
    } catch (error) {
      console.error(error);
    }
  }
    
  render() {
    return (
      <SafeAreaView
        style={{
          backgroundColor: "#F5FCFF",
          flex: 1
        }}
      >
        <ScrollView contentContainerStyle={styles.container}>
          <KeyboardAvoidingView style={styles.containerStyle} behavior="padding" >
            <Text style={styles.title}>Get Started Now</Text>
            <Image
              style={{
                width: (container_width * 2) / 3,
                resizeMode: "contain"
              }}
              source={require("../assets/images/login.png")}
            />
            <View style={{ marginTop: 15 }}>
              <View style={{ marginBottom: 18 }}>
                <Text
                  style={{
                    marginBottom: 5,
                    marginLeft: 5,
                    color: "#8B959A"
                  }}
                >
                  First Name
                </Text>
                <TextInput
                  onChangeText={text =>
                    this.setState({
                      first_name: text
                    })
                  }
                  style={styles.input}
                  placeholder="First Name"
                />
              </View>
              <View style={{ marginBottom: 18 }}>
                <Text
                  style={{
                    marginBottom: 5,
                    marginLeft: 5,
                    color: "#8B959A"
                  }}
                >
                  Last Name
                </Text>
                <TextInput
                  onChangeText={text =>
                    this.setState({
                      last_name: text
                    })
                  }
                  style={styles.input}
                  placeholder="Last Name"
                />
              </View>
              <View style={{ marginBottom: 18 }}>
                <Text
                  style={{
                    marginBottom: 5,
                    marginLeft: 5,
                    color: "#8B959A"
                  }}
                >
                  E-mail
                </Text>
                <TextInput
                  onChangeText={text =>
                    this.setState({
                      email: text
                    })
                  }
                  style={styles.input}
                  placeholder="E-mail"
                />
              </View>
              <View style={{ marginBottom: 18 }}>
                <Text
                  style={{
                    marginBottom: 5,
                    marginLeft: 5,
                    color: "#8B959A"
                  }}
                >
                  Password
                </Text>
                <TextInput
                  secureTextEntry={true}
                  onChangeText={text =>
                    this.setState({
                      password: text
                    })
                  }
                  style={styles.input}
                  placeholder="Password"
                />
              </View>
            </View>

            <TouchableOpacity
              style={styles.login}
              activeOpacity={0.9}
              onPress={this.onRegister.bind(this)}
            >
              <Text style={styles.login_text}>Login to my account</Text>
            </TouchableOpacity>
          </KeyboardAvoidingView>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    flexGrow: 1
  },
  container: {
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 30
  },
  title: {
    fontWeight: "bold",
    fontSize: 24,
    color: "#3D4A59",
    marginBottom: 10
  },
  login: {
    width: container_width,
    justifyContent: "center",
    alignItems: "center",
    height: 50,
    backgroundColor: "#F24A55",
    borderRadius: 8,
    marginTop: 20
  },
  login_text: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 16
  },
  input: {
    borderColor: "#8B959A",
    borderWidth: 0.5,
    backgroundColor: "#fff",
    height: 40,
    width: container_width,
    borderRadius: 5,
    paddingLeft: 10
  }
});