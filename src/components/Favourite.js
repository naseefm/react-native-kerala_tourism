import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

export default class Favourite extends Component {
  render(){
    return (
      <View style={styles.Container}>
        <Text style={styles.text}>Favourite page</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  Container: {
    height: "100%",
    backgroundColor: "#f5f5f5",
    alignItems: "center",
    justifyContent: "center"
  },

});