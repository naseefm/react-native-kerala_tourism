import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  SafeAreaView,
  Image,
  View,
  ScrollView,
  TouchableOpacity
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import axios from "axios";
import PlaceGalleryItem from "./includes/PlaceGalleryItem";

export default class Place extends Component {
  state = {
    place: {
      name: "",
      image:
        "http://traveller.demotechpe.talrop.com/static/images/placeholder.png",
      id: "",
      gallery: []
    }
  };
  componentDidMount() {
    name = this.props.navigation.getParam("name", "");
    image = this.props.navigation.getParam("image", "");
    pk = this.props.navigation.getParam("pk", "");
    if (!pk) {
      pk = this.props.navigation.state.params.id;
    }
    this.setState({
      place: {
        name: name,
        image: image,
        id: pk,
        gallery: []
      }
    });

    let place_url =
      "http://traveller.demotechpe.talrop.com/api/v1/places/view/" + pk + "/";
    axios
      .get(place_url)
      .then(response => {
        this.setState({
          place: response.data.data
        });
      })
      .catch(function(error) {
        console.error(error);
      });
  }

  renderGalleryImages() {
    return this.state.place.gallery.map(gallery => (
      <PlaceGalleryItem key={gallery.id} image={gallery.image} />
    ));
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <View style={styles.topArea}>
            <Image
              source={{
                uri: this.state.place.image
              }}
              style={styles.mainImage}
            />
            <View style={styles.topBox}>
              <TouchableOpacity
                onPress={() => this.props.navigation.goBack()}
                style={styles.topBoxIcon}
              >
                <Icon
                  name="arrow-left"
                  size={15}
                  color="#FF2D55"
                  style={styles.topBoxIcon}
                />
              </TouchableOpacity>
              <Icon name="bookmark-o" size={15} color="#FF2D55" />
            </View>
          </View>
          <View style={styles.Content}>
            <View style={styles.locationBox}>
              <Icon
                name="map-marker"
                size={15}
                color="#FF2D55"
                style={styles.locationBoxIcon}
              />
              <Text style={styles.locationBoxText}>
                {this.state.place.category_name}
              </Text>
            </View>
            <Text style={styles.Title}>{this.state.place.name}</Text>
            <View style={styles.reviewBox}>
              <Icon
                name="star-o"
                size={15}
                color="#FF2D55"
                style={styles.reviewBoxIcon}
              />
              <Text style={styles.reviewBoxText}>24 Reviews</Text>
            </View>
            <Text style={styles.textContent}>
              {this.state.place.description}
            </Text>
            <View style={styles.GalleryBox}>
              <Text style={styles.GalleryTitle}>Photos</Text>
              <View style={styles.GalleryImages}>
                {this.renderGalleryImages()}
              </View>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
    backgroundColor: "#fff",
    flex: 1
  },
  topArea: {
    position: "relative",
    width: "100%",
    height: 300
  },
  mainImage: {
    width: null,
    height: null,
    flex: 1
  },
  topBox: {
    position: "absolute",
    top: 0,
    left: 0,
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 15,
    marginVertical: 20,
    width: "100%"
  },
  topBoxIcon: {
    color: "#fff",
    fontSize: 20
  },
  Content: {
    marginTop: 20,
    paddingHorizontal: 15
  },
  locationBox: {
    flexDirection: "row",
    alignItems: "center"
  },
  locationBoxIcon: {
    color: "#BEC2CE",
    marginRight: 10
  },
  locationBoxText: {
    color: "#BEC2CE",
    textTransform: "uppercase",
    fontSize: 12
  },
  Title: {
    color: "#1E2432",
    fontWeight: "bold",
    fontSize: 30,
    marginTop: 10
  },
  reviewBox: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 10
  },
  reviewBoxIcon: {
    color: "#BEC2CE",
    marginRight: 10
  },
  reviewBoxText: {
    color: "#BEC2CE",
    fontSize: 12
  },
  textContent: {
    fontSize: 17,
    color: "#1E2432",
    marginTop: 15,
    lineHeight: 26
  },
  GalleryBox: {
    marginTop: 15
  },
  GalleryTitle: {
    fontSize: 15,
    fontWeight: "bold",
    textTransform: "uppercase",
    marginBottom: 15
  },
  GalleryImages: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-between"
  }
});
