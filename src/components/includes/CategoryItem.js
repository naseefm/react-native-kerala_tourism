import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';

export default class CategoryItem extends Component {
  render(){
    return (
        <TouchableOpacity style={styles.gridViewItem}>
            <ImageBackground source={{uri: this.props.image}} style={styles.gridViewItemImage}>
                <Text style={styles.gridViewItemImageText}>
                    {this.props.name}
                </Text>
            </ImageBackground>
        </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
    gridView: {
        paddingHorizontal: 15,
        marginTop: 20,
        flex: 1,
        justifyContent: "space-between",
        flexDirection: "row",
        flexWrap: "wrap",
    },
      gridViewItem: {
        width: "31%",
        height: 80,
        marginBottom: 15
    },
      gridViewItemImage: {
        width: null,
        height: null,
        flex: 1,
        borderRadius: 10,
        overflow: "hidden",
        justifyContent: "center",
        alignItems: "center",
    },
      gridViewItemImageText: {
        color: "#fff",
        fontWeight: "bold",
        fontSize: 15
    },
});