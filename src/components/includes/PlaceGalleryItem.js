import React, { Component } from "react";
import { StyleSheet, View, Image } from "react-native";

export default class PlaceGalleryItem extends Component {
  render() {
    return (
      <View style={styles.GalleryImageContainer}>
        <Image style={styles.GalleryImage} source={{ uri: this.props.image }} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  GalleryImageContainer: {
    width: "30%",
    height: 130,
    borderRadius: 5,
    overflow: "hidden",
    marginBottom: 15
  },
  GalleryImage: {
    width: null,
    height: null,
    backgroundColor: "red",
    flex: 1
  }
});
