import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';

export default class MainScrollCard extends Component {
  render(){
    return (
        <TouchableOpacity style={styles.mainScrollcardItem} onPress={() => this.props.navigation.navigate("Place",{
            image: this.props.image,
            name: this.props.name,
            pk: this.props.pk,
        })}>
            <ImageBackground source={{uri: this.props.image}} style={styles.mainScrollcardItemImage}>
                <Text style={styles.mainScrollcardItemImageText}>
                    {this.props.name}
                </Text>
            </ImageBackground>
        </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
    mainScrollcard: {
        marginLeft: 15,
        marginTop: 20,
    },
    mainScrollcardItem: {
        width: 220,
        height: 130,
        marginRight: 15,
    },
    mainScrollcardItemImage: {
        width: null,
        height: null,
        flex: 1,
        borderRadius: 8,
        overflow: "hidden",
        padding: 10,
        justifyContent: "flex-end",
    },
    mainScrollcardItemImageText: {
        color: "#fff",
        fontSize: 14,
        fontFamily: "WorkSans-Regular",
    },
});