import React, { Component } from "react";
import { StyleSheet, Text, Dimensions, View } from "react-native";
var { height, width } = Dimensions.get("window");
import AsyncStorage from "@react-native-community/async-storage";

export default class PreLoading extends Component {
  constructor() {
    super();
    this._bootstrapAsync();
  }

  _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem("user");

    this.props.navigation.navigate(userToken ? "App" : "Auth");
  };
  render() {
    return (
      <View style={styles.popupContainer}>
        <View style={styles.BottomContainer}>
          <Text style={styles.BottomText}>Traveller</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  popupContainer: {
    position: "absolute",
    zIndex: 5,
    width: width,
    height: height,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#60c2a2"
  },
  LottieStyle: {
    width: 150
  },
  BottomContainer: {
    position: "absolute",
    bottom: 50
  },
  BottomText: {
    fontSize: 25,
    color: "#fff"
  }
});
