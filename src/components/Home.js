import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Image,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import axios from "axios";
import { ceil } from 'react-native-reanimated';
import MainScrollCard from './includes/MainScrollCard';
import CategoryItem from './includes/CategoryItem';
import AsyncStorage from '@react-native-community/async-storage';

export default class Home extends Component {
  state = {
    places: [],
    categories: []
  }
  _signOutAsync = async () => {
    await AsyncStorage.clear();
    this.props.navigation.navigate("Auth");
  };
  componentDidMount() {
    let place_url = "http://traveller.demotechpe.talrop.com/api/v1/places/";
    axios
      .get(place_url)
      .then(response => {
        this.setState({
          places: response.data.data
        });
      })
      .catch(function(error) {
        console.error(response)
      });

      let category_url = "http://traveller.demotechpe.talrop.com/api/v1/places/categories/";
    axios
      .get(category_url)
      .then(response => {
        this.setState({
          categories: response.data.data
        });
      })
      .catch(function(error) {
        console.error(response)
      });
  }

  renderSlider() {
    return this.state.places.slice(0, 4).reverse().map(place =>
        <MainScrollCard
          {...this.props}
          key={place.id}
          pk={place.id}
          image={place.image}
          name={place.name}
        />
      );
  }
  renderCategories() {
    return this.state.categories.map(category =>
        <CategoryItem
          {...this.props}
          key={category.id}
          image={category.image}
          name={category.name}
        />
      );
  }
  render(){
    return (
      <SafeAreaView style={styles.Container}>
        <ScrollView>
          <View style={styles.Header}>
            <View style={styles.Formview}>
              <Icon name="search" size={15} color="#000" />
              <TextInput style={styles.Formtext} placeholder="Search"/>
            </View>
            {/* <TouchableOpacity style={styles.profileView} onPress={() => this._signOutAsync()}>
              <Image style={styles.userImage} source={require("../assets/images/naseef.jpg")} />
            </TouchableOpacity> */}
            <Icon 
              name="bars" 
              size={27} color="#444"  
              style={{marginLeft: 20}}
              onPress={() => (this.props.navigation.openDrawer())} />
          </View>
          <View style={styles.discoverPlaces}>
            <View style={styles.maincardHeader}>
              <Text style={styles.maincardHeaderText}>
                Discover new places
              </Text>
              <TouchableOpacity style={styles.maincardHeaderButton}>
              <Text style={styles.maincardHeaderButtonText}>
                View All
              </Text>
              </TouchableOpacity>
            </View>
            <ScrollView style={styles.mainScrollcard} horizontal={true} showsHorizontalScrollIndicator={false}>
              {this.renderSlider()}
            </ScrollView>
          </View>
          <View style={styles.explore}>
            <View style={styles.maincardHeader}>
              <Text style={styles.maincardHeaderText}>
                Explore new world
              </Text>
              <TouchableOpacity style={styles.maincardHeaderButton}>
                <Text style={styles.maincardHeaderButtonText}>
                 View All
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.gridView}>
            {this.renderCategories()}
          </View>
          <View style={styles.savedPlaces}>
            <View style={styles.maincardHeader}>
              <Text style={styles.maincardHeaderText}>
                Saved places
              </Text>
              <TouchableOpacity style={styles.maincardHeaderButton}>
                <Text style={styles.maincardHeaderButtonText}>
                  View All
                </Text>
              </TouchableOpacity>
            </View>
            <ScrollView style={styles.bottomScrollcard} horizontal={true} showsHorizontalScrollIndicator={false}>
              <TouchableOpacity style={styles.bottomScrollcardItem}>
                <ImageBackground source={require("../assets/images/slider1.jpg")} style={styles.bottomScrollcardItemImage}>
                  <Icon name="bookmark" size={23} color="#ff2d55" style={styles.bottomScrollcardIcon} />
                  <Text style={styles.bottomScrollcardItemImageText}>
                    National Theatre
                  </Text>
                </ImageBackground>
              </TouchableOpacity>
              <TouchableOpacity style={styles.bottomScrollcardItem}>
                <ImageBackground source={require("../assets/images/slider2.jpg")} style={styles.bottomScrollcardItemImage}>
                  <Icon name="bookmark" size={23} color="#ff2d55" style={styles.bottomScrollcardIcon} />
                  <Text style={styles.bottomScrollcardItemImageText}>
                    National Theatre
                  </Text>
                </ImageBackground>
              </TouchableOpacity>
              <TouchableOpacity style={styles.bottomScrollcardItem}>
                <ImageBackground source={require("../assets/images/slider3.jpg")} style={styles.bottomScrollcardItemImage}>
                  <Icon name="bookmark" size={23} color="#ff2d55" style={styles.bottomScrollcardIcon} />
                  <Text style={styles.bottomScrollcardItemImageText}>
                    National Theatre
                  </Text>
                </ImageBackground>
              </TouchableOpacity>
              <TouchableOpacity style={styles.bottomScrollcardItem}>
                <ImageBackground source={require("../assets/images/slider4.jpeg")} style={styles.bottomScrollcardItemImage}>
                  <Icon name="bookmark" size={23} color="#ff2d55" style={styles.bottomScrollcardIcon} />
                  <Text style={styles.bottomScrollcardItemImageText}>
                    National Theatre
                  </Text>
                </ImageBackground>
              </TouchableOpacity>
            </ScrollView>
          </View>
        </ScrollView>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  Container: {
    height: "100%",
    backgroundColor: "#f1f2f6",
  },
  Header: {
    paddingTop: 7,
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 15,
    alignItems: "center",
  },
  Formview: {
    backgroundColor: "#fff",
    borderWidth: 1,
    borderColor: "#eaecef",
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 20,
    paddingVertical: 3,
    borderRadius: 8,
  },
  Formtext: {
    marginLeft: 10,
    width: "95%",
    height: 40,
  },
  profileView: {
    marginLeft: 15,
  },
  userImage: {
    width: 30,
    height: 30,
    borderColor: "#707070",
    borderWidth: 1,
    borderRadius: 15,
  },
  discoverPlaces: {
    marginTop: 30,
  },
  maincardHeader: {
    paddingHorizontal: 15,
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  maincardHeaderText: {
    fontSize: 18,
    fontFamily: "WorkSans-Bold",
  },
  maincardHeaderButton: {
    backgroundColor: "#ff2d55",
    paddingHorizontal: 25,
    paddingVertical: 7,
    borderRadius: 3
  },
  maincardHeaderButtonText: {
    color: "#fff",
    fontSize: 15,
    fontFamily: "WorkSans-Regular",
  },
  mainScrollcard: {
    marginLeft: 15,
    marginTop: 20,
  },
  mainScrollcardItem: {
    width: 220,
    height: 130,
    marginRight: 15,
  },
  mainScrollcardItemImage: {
    width: null,
    height: null,
    flex: 1,
    borderRadius: 8,
    overflow: "hidden",
    padding: 10,
    justifyContent: "flex-end",
  },
  mainScrollcardItemImageText: {
    color: "#fff",
    fontSize: 14,
    fontFamily: "WorkSans-Regular",
  },
  explore: {
    marginTop: 30,
  },
  gridView: {
    paddingHorizontal: 15,
    marginTop: 20,
    flex: 1,
    justifyContent: "space-between",
    flexDirection: "row",
    flexWrap: "wrap"
,  },
  gridViewItem: {
    width: "31%",
    height: 80,
    marginBottom: 15
  },
  gridViewItemImage: {
    width: null,
    height: null,
    flex: 1,
    borderRadius: 10,
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
  },
  gridViewItemImageText: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 15
  },
  savedPlaces: {
    marginVertical: 15,

  },
  bottomScrollcard: {
    marginLeft: 15,
    marginTop: 20,
  },
  bottomScrollcardItem: {
    width: 150,
    height: 100,
    marginRight: 15,
  },
  bottomScrollcardItemImage: {
    width: null,
    height: null,
    flex: 1,
    borderRadius: 8,
    overflow: "hidden",
    padding: 10,
    justifyContent: "space-between",
  },
  bottomScrollcardItemImageText: {
    color: "#fff",
    fontSize: 13,
    fontWeight: "bold"
  },
  bottomScrollcardIcon: {
    textAlign: "right",
  }
});
